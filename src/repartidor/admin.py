from django.contrib import admin
from .models import Repartidor
from .forms import RepaModelForm

class AdminRepartidor(admin.ModelAdmin):
    list_display = ["nombre","fono","mail","genero","tipo_licencia"]
    form = RepaModelForm
    list_filter = ["tipo_licencia"]
    search_fields = ["nombre","tipo_licencia"]
    class Meta:
        model = Repartidor



admin.site.register(Repartidor,AdminRepartidor)
# Register your models here.
