from django.shortcuts import render
from .forms import RepaModelForm,ContactForm
from django.conf import settings
from django.core.mail import send_mail
from .models import Repartidor

def inicio(request):
    form = RepaModelForm(request.POST or None)
    titulo = "BIENVENIDOS AL REGISTRO DE REPARTIDOR"
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" % (request.user)

    contexto = {
        "el_titulo": titulo,
        "el_formulario": form,
    }

    if form.is_valid():
        instancia = form.save(commit=False)  # Esta línea impide la creación de objetos
        if not instancia.nombre:
            instancia.nombre = "REPARTIDOR SIN NOMBRE"
        instancia.save()

        contexto = {"el_titulo": "Gracias %s!" % (instancia.nombre)

                    }

        print(instancia)

    return render(request, "repartidor.html", contexto)



def contacto(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        formulario_nombre = form.cleaned_data.get("nombre")
        formulario_email = form.cleaned_data.get("email")
        formulario_mensaje = form.cleaned_data.get("mensaje")
        asunto = "Formulario de contacto web"
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, "correo_destinatario@dominio.com"]
        email_mensaje = "Enviado por %s – Correo: %s – Mensaje: %s" % (formulario_nombre,formulario_email,formulario_mensaje)
        send_mail(asunto,
                  email_mensaje,
                  email_from,
                  email_to,
                  fail_silently=False
                  )

    contexto = {
        "el_contacto": form,
    }
    return render(request, "contacto.html", contexto)

# Create your views here.
