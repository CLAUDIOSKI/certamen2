from django.db import models



class Repartidor(models.Model):
    id_repartidor = models.AutoField (primary_key=True,unique=True)
    nombre = models.CharField(max_length=80,null=True,blank=True)
    rut = models.CharField(max_length=12,null=True)
    fono = models.IntegerField()
    mail = models.EmailField()
    genero = models.CharField(max_length=80, choices=(('M', 'Masculino'),('F', 'Femenino')))
    tipo_licencia = models.CharField(max_length=80, choices=(('A', 'Clase A'),('B', 'Clase B'),('N','No tiene')))

    def __str__(self):
        return self.nombre

# Create your models here.
