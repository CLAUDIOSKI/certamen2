from django import forms
from .models import Repartidor

class RepaModelForm(forms.ModelForm):
    class Meta:
        model = Repartidor
        campos = ["nombre","rut","fono","mail","genero","tipo_licencia"]
        exclude = ()

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) <= 5:
                raise forms.ValidationError("El nombre del repartidor  no puede ser menos a 5 caracteres")
            return nombre
        else:
            return nombre


class ContactForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    email = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) <= 5:
                raise forms.ValidationError("El nombre del repartidor  no puede ser menos a 5 caracteres")
            return nombre
        else:
            return nombre

    def clean_email(self):
        email = self.cleaned_data.get("email")
        email_base, proveedor = email.split("@")
        dominio, extension = proveedor.split(".")
        if not extension == "cl" and  not extension == "com":
            raise forms.ValidationError("Solo se aceptan correos con extensión.cl o .com")
        return email