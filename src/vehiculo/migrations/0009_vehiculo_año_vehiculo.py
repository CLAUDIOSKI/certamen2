# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-16 01:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehiculo', '0008_remove_vehiculo_año_vehiculo'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehiculo',
            name='año_vehiculo',
            field=models.PositiveIntegerField(default=2012),
        ),
    ]
