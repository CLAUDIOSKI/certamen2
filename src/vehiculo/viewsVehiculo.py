from django.shortcuts import render
from .forms import VehiModelForm
from .models import Vehiculo


def inicio(request):
    form = VehiModelForm(request.POST or None)
    titulo = "BIENVENIDOS AL REGISTRO DE VEHICULO"
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" % (request.user)

    contexto = {
        "el_titulo": titulo,
        "el_formulario": form,
    }

    if form.is_valid():
        instancia = form.save(commit=False)  # Esta línea impide la creación de objetos
        if not instancia.marca:
            instancia.marca = "AUTO SIN MARCA"
        instancia.save()

        contexto = {"el_titulo": "Gracias %s!" % (request.user)

                    }

        print(instancia)

    return render(request, "vehiculo.html", contexto)




# Create your views here.
