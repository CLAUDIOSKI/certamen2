from django.db import models
from repartidor.models import Repartidor

class Vehiculo(models.Model):
    id_vehiculo = models.AutoField(primary_key=True,unique=True)
    tipo = models.CharField(max_length=80,choices=(('M', 'Moto'),('C', 'Camioneta'),('A','Auto')))
    marca = models.CharField(max_length=80,blank=True)
    modelo = models.CharField(max_length=80)
    año_vehiculo = models.PositiveIntegerField(default=2012)
    patente = models.CharField(max_length=80,unique=True)
    id_repartidor = models.ForeignKey(Repartidor)


    def __str__(self):
        return self.modelo

# Create your models here.
