from django import forms
from .models import Vehiculo

class VehiModelForm(forms.ModelForm):
    class Meta:
        model = Vehiculo
        campos = ["tipo","marca","modelo","año_vehiculo","patente","id_repartidor"]
        exclude = ()

    def clean_año_vehiculo(self):
        año = self.cleaned_data.get("año_vehiculo")

        if año < 2012:
            raise forms.ValidationError("El año del vehiculo no puede ser menor a 2012")
        elif año == 0:
            raise forms.ValidationError("El año del vehiculo no puede ser igual a 0 ")
        return año


    def clean_modelo(self):
        modelo = self.cleaned_data.get("modelo")
        if modelo:
            if len(modelo) <= 5:
                raise forms.ValidationError("El modelo del vehiculo  no puede contener menos de 5 caracteres")
            return modelo
        else:
            raise forms.ValidationError("Este campo es obligatorio")
