from django.contrib import admin
from .models import Vehiculo
from .forms import VehiModelForm
class AdminVehiculo(admin.ModelAdmin):
    list_display = ["tipo","marca","modelo","año_vehiculo","patente","id_repartidor"]
    form = VehiModelForm
    list_filter = ["tipo"]
    search_fields = ["tipo","marca"]
    class Meta:
        model = Vehiculo




admin.site.register(Vehiculo,AdminVehiculo)

# Register your models here.
